import * as plugins from './smartfeed.plugins';

export interface IFeedOptions {
  domain: string;
  title: string;
  description: string;
  category: string;
  company: string;
  companyEmail: string;
  companyDomain: string;
}

export interface IFeedItem {
  title: string;
  timestamp: number;
  url: string;
  authorName: string;
  imageUrl: string;
  content: string;
}

export class Feed {
  options: IFeedOptions;
  items: IFeedItem[] = [];
  constructor(optionsArg: IFeedOptions) {
    this.options = optionsArg;
  }

  public addItem(itemArg: IFeedItem) {
    this.items.push(itemArg);
  }

  private getFeedObject() {
    const feed = new plugins.feed.Feed({
      copyright: `All rights reserved, ${this.options.company}`,
      id: `https://${this.options.domain}`,
      link: `https://${this.options.domain}`,
      title: this.options.title,
      author: {
        name: this.options.company,
        email: this.options.companyEmail,
        link: this.options.companyEmail
      },
      description: this.options.description,
      generator: '@pushrocks/smartfeed',
      language: "en"
    });

    feed.addCategory(this.options.category);
    for (const itemArg of this.items) {
      feed.addItem({
        title: itemArg.title,
        date: new Date(itemArg.timestamp),
        link: itemArg.url.replace(/&/gm, '&amp;'),
        image: itemArg.imageUrl.replace(/&/gm, '&amp;'),
        content: itemArg.content,
        author: [{
          name: itemArg.authorName
        }]
      });
    }
    return feed;
  }

  public exportRssFeedString(): string {
    return this.getFeedObject().rss2();
  }

  public exportAtomFeed(): string {
    return this.getFeedObject().atom1();
  }

  public exportJsonFeed(): string {
    return this.getFeedObject().json1();
  }
}