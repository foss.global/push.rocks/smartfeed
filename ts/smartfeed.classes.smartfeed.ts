import { Feed, IFeedOptions } from './smartfeed.classes.feed';
import * as plugins from './smartfeed.plugins';

export class Smartfeed {
  public createFeed(optionsArg: IFeedOptions) {
    const feedVersion = new Feed(optionsArg);
    return feedVersion;
  }

  /**
   * creates a feed from a standardized article object (@tsclass/tsclass).content.IArticle
   */
  public async createFeedFromArticleArray(optionsArg: IFeedOptions, articleArray: plugins.tsclass.content.IArticle[]): Promise<string> {
    const feed = this.createFeed(optionsArg);
    for (const article of articleArray) {
      feed.addItem({
        authorName: `${article.author.firstName} ${article.author.surName}`,
        timestamp: article.timestamp,
        imageUrl: article.featuredImageUrl,
        title: article.title,
        url: article.url,
        content: article.content
      });
    }
    const feedXmlString = feed.exportAtomFeed();
    return feedXmlString;
  }

  /**
   * allows the parsing of a rss feed string
   * @param rssFeedString
   */
  public async parseFeedFromString(rssFeedString: string) {
    const parser = new plugins.rssParser();
    const resultingFeed = await parser.parseString(rssFeedString);
    return resultingFeed;
  }

  /**
   * allows the parsing of a feed from urls
   * @param urlArg
   */
  public async parseFeedFromUrl(urlArg: string) {
    const parser = new plugins.rssParser();
    const resultingFeed = await parser.parseURL(urlArg);
    return resultingFeed;
  }
}