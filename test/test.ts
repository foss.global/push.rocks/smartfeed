import { expect, tap } from '@pushrocks/tapbundle';
import * as smartfeed from '../ts/index';

let testSmartFeed: smartfeed.Smartfeed;

tap.test('should create a feedVersion', async () => {
  testSmartFeed = new smartfeed.Smartfeed();
  expect(testSmartFeed).to.be.instanceOf(smartfeed.Smartfeed);
});

tap.test('should create a feed', async () => {
  const feed = testSmartFeed.createFeed({
    domain: 'central.eu',
    title: 'central.eu - ideas for Europe',
    category: 'Politics',
    company: 'Lossless GmbH',
    companyDomain: 'https://lossless.com',
    companyEmail: 'hello@lossless.com',
    description: 'ideas for Europe',
  });
  feed.addItem({
    title: 'A better European Union',
    authorName: 'Phil',
    imageUrl: 'https://central.eu/someimage.png',
    timestamp: Date.now(),
    url: 'https://central.eu/article/somearticle',
    content: 'somecontent'
  });
  const rssFeed = feed.exportRssFeedString();
  console.log(rssFeed);
  const parsedFeed = await testSmartFeed.parseFeedFromString(rssFeed);
  console.log(parsedFeed);
});

tap.test('should parse a Url', async () => {
  const result = await testSmartFeed.parseFeedFromUrl('https://www.theverge.com/rss/index.xml');
  // console.log(result);
});

tap.start();
